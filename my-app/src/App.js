import List from "./pages/List";
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import Houses from "./pages/Houses";
import ErrorPage from "./pages/ErrorPage";

    const router = createBrowserRouter(
        [
            {
                path: '/',
                element: <List/>,
                errorElement: <ErrorPage/>
            },
            {
                path: '/House/:id',
                element: <Houses/>,
            },
            {
                path: '/main_page/:page',
                element: <List/>,
            },
        ])

    const App = () => {
        return <RouterProvider router={router}/>;
    }

export default App;
