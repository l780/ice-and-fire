import React, {useContext, useEffect, useState} from 'react';
import {getCharacters, getPages} from "../http";
import {Link} from "react-router-dom";
import Pages from "../component/Pages";
import {observer} from "mobx-react-lite";
import {Context} from "../index";

const List = observer(() => {
    const {characters} = useContext(Context)
    const [numberOfNewCharacters, setNumberOfNewCharacters] = useState(0)
    const [culture, setCulture] = useState('')
    const [gender, setGender] = useState('')


    useEffect(() => {
        getCharacters().then(res => {
            characters.setCharacters(res)
            characters.setFilteredCharacters(res)
        })
    }, [])

    useEffect(() => {
        getPages(characters.page).then(res => {
            characters.setCharacters(res)
            characters.setFilteredCharacters(res)
        })
    }, [characters.page])

    useEffect(() => {
        if (gender == 0) {
            characters.setFilteredCharacters(characters.characters)
        } else {
            characters.setFilteredCharacters(characters.characters.filter(item => item.gender === gender))
        }
    }, [gender]);

    useEffect(() => {
        const getData = setTimeout(() => {
            if (culture !== '') {
                characters.setFilteredCharacters(characters.filteredCharacters.filter(item => item.culture === culture))
            }
        }, 2000)

        return () => clearTimeout(getData)
    }, [culture])

    function formatDateOfDie(dateBorn, dateDied) {
        if (dateBorn !== '') {
            return `Died at age of ${dateDied.replace(/[^0-9]/g, "").slice(0, 3) - dateBorn.replace(/[^0-9]/g, "").slice(0, 3)}`
        }
        return 'Died, age unknown'
    }


    const AddNewCharacters = async () => {
        characters.setCharacters(await getCharacters(numberOfNewCharacters))
        characters.setFilteredCharacters(await getCharacters(numberOfNewCharacters))
    }
    console.log('characters', characters)

    return (
        <div className="container mt-3">
            <h2>Character List</h2>
            <div className="mb-3 w-50">

                <p>Filter by culture</p>
                <div className="form-outline mb-3">
                    <input
                        placeholder="Enter culture of characters"
                        type="text"
                        className="form-control"
                        value={culture}
                        onChange={e => setCulture(e.target.value)}
                    />
                    <label style={{color: "red"}} className="form-label">
                    </label>
                </div>

                <p>Filter by gender</p>
                <select
                    className="form-select mb-3"
                    aria-label="Filter by gender"
                    onClick={e => setGender(e.target.value)}
                >
                    <option value={0}>Any</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>

                <p>Add new characters</p>
                <div className="d-flex align-items-center">
                    <select
                        className="form-select w-50"
                        style={{marginRight: 20}}
                        aria-label="Add new characters"
                        onClick={(e) => setNumberOfNewCharacters(e.target.value)}
                    >
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </select>

                    <div className="d-flex justify-content-center">
                        <button
                            onClick={AddNewCharacters}
                            type="button"
                            className="btn btn-primary btn-block btn-lg gradient-custom-4">
                            Amount of characters
                        </button>
                    </div>
                </div>

            </div>

            <table className="table table-bordered">
                <thead>
                <tr>
                    <th>Firstname</th>
                    <th>Alive</th>
                    <th>Gender</th>
                    <th>Culture</th>
                    <th>Allegiances</th>
                    <th>Books</th>
                </tr>
                </thead>
                <tbody>

                {characters.filteredCharacters.map((item, index) =>
                    <tr key={index}>
                        {item.name && item.aliases[0].length !== 0 ?
                            <td>{item.name.concat(', ', item.aliases)}</td>
                            :
                            <td>{item.aliases[0].length !== 0 ? item.aliases : item.name}</td>
                        }

                        <td>{item.died === '' ? 'Yes' : formatDateOfDie(item.born, item.died)}</td>
                        <td>{item.gender === '' ? 'Unknown' : item.gender}</td>
                        <td>{item.culture === '' ? 'Unknown' : item.culture}</td>
                        <td>{item.allegiances.length !== 0 ? <Link to={`/House/${item.allegiances[0].replace(/[^0-9]/g, "")}`}>Houses</Link> : 'no Allegiances'}</td>
                        <td>{item.books.length}</td>
                    </tr>
                )}

                </tbody>
            </table>

            <Pages/>
        </div>
    );
});

export default List;