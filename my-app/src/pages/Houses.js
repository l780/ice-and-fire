import React, {useContext, useEffect} from 'react';
import {getHouse} from "../http";
import {useParams} from "react-router-dom";
import {Context} from "../index";
import {toJS} from 'mobx';
import {observer} from "mobx-react-lite";


const Houses = observer(() => {
    const {characters} = useContext(Context)
    const houseId = useParams()
    useEffect(() => {
        getHouse(houseId.id).then(res => characters.setHouse(res))
    }, [])
    console.log(characters.house)
    const house = toJS(characters.house)
    console.log('house', house)

    if (characters.house.length === 0) {
             return <h1>PreLoader</h1>
    }
        return (
            <div className="container mt-3 d-flex justify-content-center flex-column">
                <table className="table table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Region</th>
                        <th>Coat of Arms</th>
                        <th>Words</th>
                        <th>Titles</th>
                        <th>Seats</th>
                        <th>Has died out</th>
                        <th>Has overlord</th>
                        <th>Number of Cadet Branches</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{house.name}</td>
                        <td>{house.region}</td>
                        <td>{house.coatOfArms}</td>
                        <td>{house.words}</td>
                        <td>{house.titles}</td>
                        <td>{house.seats}</td>
                        <td>{house.diedOut === '' ? 'No' : house.diedOut}</td>
                        <td>{house.overlord === '' ? 'No' : 'Yes'}</td>
                        <td>{house.cadetBranches === [] ? 0 : house.cadetBranches.length}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        );
});

export default Houses;