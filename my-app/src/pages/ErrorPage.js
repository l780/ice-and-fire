import React from 'react';

const ErrorPage = () => {
    return (
        <div>
            <h1>404</h1>
            <h2>Page not found</h2>
            <p>Go to home</p>
        </div>
    );
};

export default ErrorPage;