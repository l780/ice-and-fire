import React, {createContext} from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import DefaultStore from "./store/DefaultStore";

const root = ReactDOM.createRoot(document.getElementById('root'));
export const Context = createContext(null);
root.render(
    <Context.Provider value={{
        characters: new DefaultStore()
    }}>
    <React.StrictMode>
        <App/>
    </React.StrictMode>
        </Context.Provider>
);
