import {makeAutoObservable} from "mobx";

export default class DefaultStore {
    constructor() {
        this._characters = [];
        this._filteredCharacters = [];
        this._page = 1;
        this._house = [];
        // this._filteredCharactersByCulture = [];
        makeAutoObservable(this)
    }


    setCharacters(data) {
        this._characters = data
    }
    setHouse(data) {
        this._house = data
    }
    setFilteredCharacters(data) {
        this._filteredCharacters = data
    }
    // setFilteredCharactersByCulture(data) {
    //     this._filteredCharactersByCulture = data
    setPage(data) {
        this._page = data
    }
    // }
    addCharacters(data) {
        this._characters = [...this._characters, ...data]
    }

    get characters() {
        return this._characters
    }

    get house() {
        return this._house
    }
    get filteredCharacters() {
        return this._filteredCharacters
    }
    get page() {
        return this._page
    }
    // get filteredCharactersByCulture() {
    //     return this._filteredCharactersByCulture
    // }
}