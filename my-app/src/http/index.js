import axios from "axios";

export const getPages = async (number) => {
    const {data} = await axios.get(`https://www.anapioficeandfire.com/api/characters?page=${number}&pageSize=10`)
    return data
}
export const getHouse = async (id) => {
    const {data} = await axios.get(`https://www.anapioficeandfire.com/api/houses/${id}`)
    return data
}
export const getCharacters = async (number) => {
    const {data} = await axios.get(`https://www.anapioficeandfire.com/api/characters?page=1&pageSize=${number}`)
    return data
}