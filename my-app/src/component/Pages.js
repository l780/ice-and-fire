import React, {useContext} from 'react';
import {observer} from "mobx-react-lite";
import Pagination from 'react-bootstrap/Pagination';
import {Container} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import {Context} from "../index";

const Pages = observer(() => {
    const {characters} = useContext(Context)
    const navigate = useNavigate()
    let currentPage = characters.page;
    console.log('currentPage', currentPage)
    console.log(characters)
    const nextNav = async ()  => {
        characters.setPage(currentPage + 1)
        navigate(`/main_page/:${currentPage + 1}`)
    }
    const prevNav = async ()  => {
        characters.setPage(currentPage - 1)
        navigate(`/main_page/:${currentPage - 1}`)
    }
    const firstNav = async ()  => {
        characters.setPage(1)
        navigate(`/main_page/:${1}`)
    }
    const lastNav = async ()  => {
        characters.setPage(214)
        navigate(`/main_page/:${214}`)
    }


    return (
        <Container style={{marginTop: 30}} className='d-flex justify-content-center form-outline mb-3"'>
            <Pagination>
                <Pagination.First onClick={() => firstNav()}/>
                <Pagination.Prev onClick={() => prevNav()}/>

                <Pagination.Next onClick={() => nextNav()}/>
                <Pagination.Last  onClick={() => lastNav()}/>
            </Pagination>
        </Container>
    );
});

export default Pages;